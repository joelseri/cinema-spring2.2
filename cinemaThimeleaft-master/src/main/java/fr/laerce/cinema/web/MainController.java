package fr.laerce.cinema.web;

import fr.laerce.cinema.dao.DataModel;
import fr.laerce.cinema.dao.FilmsDao;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

//pour dire a springboot qu'il est un controller web on écrit cette phrase
@Controller
public class MainController{
    //on peut utiliser cette méthode avec autowired et component dans le servlet DataModel
    @Autowired
    DataModel dataModel;


    //Pour mapper la servlet,ça remplace ce que l'on met dans web.xml.
    @GetMapping("/")
    public String main(Model M){
        //on ajoute a l'objet model la clef nom et karl
        M.addAttribute ("nom","joel" );
        M.addAttribute ("films",dataModel.getFilms());
        //on return la chaine string index de façon à ouvrir index.html
        return "index";
    }
    ///////////////////////////////////////////////////////////////
    //on map vers film donc dans l'url on ecrira /film?id=1
//    @GetMapping("/film")
    //on recupere le parametre id de l'url grace à raquestparam
//    public String detail(Model m, @RequestParam("id") String id){
    ////////////////////////////////////////////////////////////////
    @GetMapping("/film/{id}")
    //on recupere id grace à pathvariable
    public String detail(Model m, @PathVariable("id") String id){
        Integer idFilm = Integer.parseInt (id);
        m.addAttribute ("film", dataModel.getById(idFilm));
        return"detail";
    }

//    on utilise properties et on recupere la valeur de l'url
    @Value( "${url}" )
    private String url;
    //deuxieme methode pour afficher  image
    @GetMapping("/affiche/{id}")
    public ResponseEntity<byte[]> getImageAsResponseEntity (HttpServletRequest request, HttpServletResponse response,@PathVariable("id") String id) {
        try {
            HttpHeaders headers = new HttpHeaders ();
            String filename=url+id;
            File i = new File (filename);
            FileInputStream in = new FileInputStream(i);
            byte[] media = IOUtils.toByteArray (in);
            headers.setCacheControl (CacheControl.noCache ().getHeaderValue ());

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<> (media, headers, HttpStatus.OK);
            return responseEntity;
        } catch (IOException e) {
            e.printStackTrace ();
        }
       return null;
 }
    @Value( "${url2}" )
    private String url2;
    //deuxieme methode pour affichezr  image
    @GetMapping("/image/{id}")
    public ResponseEntity<byte[]> getImageAsResponseEntity2 (HttpServletRequest request, HttpServletResponse response,@PathVariable("id") String id) {
        try {
            HttpHeaders headers = new HttpHeaders ();
            String filename=url2+id;
            File i = new File (filename);
            FileInputStream in = new FileInputStream(i);
            byte[] media = IOUtils.toByteArray (in);
            headers.setCacheControl (CacheControl.noCache().getHeaderValue());

            ResponseEntity<byte[]> responseEntity = new ResponseEntity<> (media, headers, HttpStatus.OK);
            return responseEntity;
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return null;
    }
    @GetMapping("/acteur/{id}")
    //on recupere id grace à pathvariable
    public String acteur(Model m, @PathVariable("id") String id){
        m.addAttribute ("acteur", dataModel.getByAf(id));
        return"acteur";}
}
